require 'spec_helper'

describe "StaticPages" do

  subject { page }

  describe "Home Page" do
    it "should have the content 'inventory'" do
      visit root_path
      should have_content('Inventory')
    end
  end
end
