Egg2::Application.routes.draw do
  root 'static_pages#home'

  match '/signup',  to: 'users#new',        via: 'get'
  match '/signin',  to: 'sessions#new',     via: 'get'
  match '/signout', to: 'sessions#destroy', via: 'delete'

  resources :users
  
  resources :invites
  resources :memberships
  resources :sessions, only: [:new, :create, :destroy]
  resources :inputs
 
  resources :groups do
    resources :products, shallow: true
  end

  resources :groups do
    resources :dash, shallow: true
  end

  resources :groups do
    resources :inputs, shallow: true
  end

  resources :groups do
    resources :outputs, shallow: true
  end

  resources :groups do
    resources :customers
  end
end
