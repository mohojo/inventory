class Product < ActiveRecord::Base
  belongs_to :group
  has_many :inputs
  has_many :customers
  has_many :outputs

  validates :description, length: { maximum: 250}
end
