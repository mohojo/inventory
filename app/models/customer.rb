class Customer < ActiveRecord::Base
  has_and_belongs_to_many :product  
  belongs_to :group

  validates :customer_area, length: { is: 0 }, length: { is: 3 }, allow_blank: true, numericality: true
  validates :customer_phone, length: { is: 0}, length: { is: 7}, allow_blank: true, numericality: true

  def human_readable_phone_number
    unless customer_phone.nil? && customer_area.nil?
      "(#{ self.customer_area }) #{ self.customer_phone.first(3)}-#{ self.customer_phone.last(4) }"
    else
      ""
    end
  end
end