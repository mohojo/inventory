# == Schema Information
#
# Table name: users
#
#  id         :integer          not null, primary key
#  name       :string(255)
#  email      :string(255)
#  created_at :datetime
#  updated_at :datetime
#

class User < ActiveRecord::Base

  has_secure_password

  before_save { self.email = email.downcase }
  before_save :create_remember_token

  validates :name, presence: true, length: { maximum: 50 }
  VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i
  validates :email, presence: true, format: { with: VALID_EMAIL_REGEX },
                    uniqueness: { case_sensitive: false }
  validates :password, presence: true, length: { minimum: 8 }
  validates :password_confirmation, presence: true
  validates :area_code, length: { is: 0 }, length: { is: 3 }, allow_blank: true, numericality: true
  validates :phone_number, length: { is: 0}, length: { is: 7}, allow_blank: true, numericality: true
 





  has_many :memberships
  has_many :groups, through: :memberships

  def human_readable_phone_number
    unless phone_number.nil? && area_code.nil?
      "(#{ self.area_code }) #{ self.phone_number.first(3)}-#{ self.phone_number.last(4) }"
    else
      ""
    end
  end
  private

    def create_remember_token
      self.remember_token = SecureRandom.urlsafe_base64
    end
end