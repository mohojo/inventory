class Input < ActiveRecord::Base
  belongs_to :product
  belongs_to :group
  validates :quantityof, allow_blank: false, numericality: true
  def formatted_time
    input_time.strftime("%b %e")
  end
end