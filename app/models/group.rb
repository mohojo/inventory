class Group < ActiveRecord::Base
  has_many :memberships
  has_many :users, through: :memberships
  has_many :products
  has_many :inputs
  has_many :outputs
  has_many :customers
end
