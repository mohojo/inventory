class InvitesController < ApplicationController
  def index
    @user_invites = Invite.where(invitee_id: current_user.id)
  end

  def new
    @invite = Invite.new
  end

  def create
    unless Membership.where(user_id: (User.find_by_email(params[:invite][:invitee_email]).id), group_id: params[:group_id]).empty?
      redirect_to :back
      flash[:failure] = "That person is already in this group."
    else
      if User.find_by_email(params[:invite][:invitee_email])
        if Invite.where(invitee_id: (User.find_by_email(params[:invite][:invitee_email]).id), group_id: params[:group_id]).empty?
          @invite = Invite.new
          @invite.group_id = params[:group_id].to_i
          @invite.inviter_id = current_user.id
          @invite.invitee_id = User.find_by_email(params[:invite][:invitee_email]).id
          if @invite.save
            flash[:success] = "Invite Sent"
            redirect_to current_user
          else
            render 'new'
          end
        else
          redirect_to :back
          flash[:failure] = "That person has a pending invite to this group already."
        end
      else
        redirect_to :back
        flash[:failure] = "No such user with that email. Please ask them to sign up."
      end
    end
  end
end
