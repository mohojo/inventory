class DashController < ApplicationController
  def index
    @user = User.find_by_email(params[:email])
    @group = Group.find(params[:group_id])    
    @all_group_products = Product.where(group_id: params[:group_id])
    @input = Input.where(product_id: params[:group_id])
  end
end