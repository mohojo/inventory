class InputsController < ApplicationController
  def index
    @inputs = Input.where(product_id: product.id, group_id: product.group.id)
  end

  def new
    @input = Input.new
    @products = Product.where(group_id: params[:group_id])
    @group = Group.find(params[:group_id])
  end

  def create
    @input = Input.new(input_params)
    @input.input_time = Date.today.beginning_of_day
    @input.group_id = params[:group_id]
      @input.save
      redirect_to group_dash_index_path  
  end

  def edit
    @input = Input.find(params[:id])
    @product = Product.find(@input.product.id)
  end

  def update
    @input = Input.find(params[:id])
    @input.update_attributes(input_params)
    redirect_to group_dash_index_path(@input.group_id)
  end
  
  def destroy
    @product = Product.find(params[:id])
    @group = @product.group
    @product.destroy
    flash[:success] = "Product removed from inventory."
    redirect_to group_products_path(@group)
  end

private

  def input_params
    params.require(:input).permit(:product_id, :quantity)
  end
end
