class GroupsController < ApplicationController

  def index
    @all_group_products = Product.where(group_id: params[:group_id])
    @group = Group.where(user_id: params[:user_id])
    if @group.nil?
      then 
      redirect_to new_group_path
    end
  end

  def show
    @group = Group.find(params[:id])
  end

  def new
    @group = Group.new
  end

  def create
    @group = Group.new(group_params)
    if @group.save
      Membership.create(group_id: @group.id, user_id: current_user.id)
      flash[:success] = "Group Created"
      redirect_to @group
    else
      render 'new'
    end
  end

  def edit
    @group = Group.find(params[:id])
    @invite = Invite.new
  end

  def update
    @group = Group.find(params[:id])
    if @group.update_attributes(group_params)
      flash[:success] = "Group Updated"
      redirect_to @group
    else
      render 'edit'
    end
  end

  private

    def group_params
      params.require(:group).permit(:name)
    end

end
