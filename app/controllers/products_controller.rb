class ProductsController < ApplicationController
  def index
    @all_group_products = Product.where(group_id: params[:group_id])
    @group = Group.find(params[:group_id])
  end

  def show
    @product = Product.find(params[:id])
    @product.group_id = params[:group_id]
  end

  def new
    @group = Group.find(params[:group_id])
    @product = Product.new

  end

  def create
    @group = Group.find(params[:group_id])
    @product = Product.new(product_params)
    @product.group_id = params[:group_id]
    if @product.save
      flash[:success] = "New product added."
      redirect_to group_products_path(@group)
    else
      flash[:error] = "Description should be less than 250 characters"
      redirect_to new_group_product_path
    end
  end

  def edit
    
    @product = Product.find(params[:id])
  end

  def update
    @product = Product.find(params[:id])
    if @product.update(product_params)
      flash[:success] = "Product changed"
    redirect_to group_products_path(@product.group)
    else
      flash[:error] = "Description should be less than 250 characters"
      redirect_to edit_product_path
    end
  end

  def destroy
    @product = Product.find(params[:id])
    @group = @product.group
    @product.destroy
    flash[:success] = "Product removed from inventory."
    redirect_to group_products_path(@group)
  end

  private

    def lowinv
      @product = Product.find(params[:id])
      if @product.numberofproduct < @product.minimumnum
  end 

  def lowinv2
    @product = Product.find(params[:id])
    @t = Time.now
    @t2 = @t.beginning_of_day
    @t3 = @t2 + 19.hours + 10.minutes
    if @t3 and lowinv
      then 
      @product.send_lowinventory_email
    end
  end end
    

  def product_params
    params.require(:product).permit(:name, :group_id, :description, :numberofproduct, :minimumnum)
  end
end
 