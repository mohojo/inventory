class MembershipsController < ApplicationController
  def create
    if Membership.where(group_id: params[:group_id], user_id: current_user.id).empty?
      Membership.create(group_id: params[:group_id], user_id: current_user.id)
      Invite.where(invitee_id: current_user.id, group_id: params[:group_id]).first.destroy
      redirect_to group_path(params[:group_id])
    end
  end

  def destroy
    Membership.find(params[:id]).destroy
    flash[:success] = "User removed from group."
    redirect_to group_path(Group.find(params[:group_id]))
  end
end
