class CustomersController < ApplicationController

  def index
    @customer = Customer.where(group_id: params[:group_id])
  end

  def new
    @group = Group.find(params[:group_id])
    @products = Product.where(group_id: params[:group_id])
    @customer = Customer.new
  end

  def create
    @customer = Customer.new(customer_params)
    @group = Group.find(params[:group_id])
    @product = Product.where(group_id: params[:group_id])
    @customer.group_id = params[:group_id]
    if @customer.save
     flash[:success] = "New customer has been added."
      redirect_to group_customers_path(@group)
    else
      flash[:error] = "Something went wrong and the customer was not added to your list."
      redirect_to new_group_customer_path
    end
  end

    private

  def customer_params
    params.require(:customer).permit(:name, :customer_area, :customer_phone, :group_id)
  end
end
