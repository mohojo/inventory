class StaticPagesController < ApplicationController
  def home
    if current_user 
      redirect_to groups_path
    else
      new_sessions_path
    end
  end
end
