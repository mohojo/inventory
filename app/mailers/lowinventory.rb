class Lowinventory < ActionMailer::Base
  default from: "inventorynotification@forthepeople.com"

	def lowinventory_email(product)
    @low = Product.where(group_id: params[:numberofproduct])
    mail(to: @low.email, subject: 'Low inventory list')
  end
end