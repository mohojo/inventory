class CreateOutputs < ActiveRecord::Migration
  def change
    create_table :outputs do |t|
      t.integer :group_id
      t.integer :product_id
      t.integer :quantity
      t.datetime :output_time
      t.timestamps
    end
  end
end
