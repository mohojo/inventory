class CreateCustomers < ActiveRecord::Migration
  def change
    create_table :customers do |t|

    t.string   :name  
    t.string   :customer_area
    t.string   :customer_phone
    t.integer  :group_id

      t.timestamps
    end
  end
end