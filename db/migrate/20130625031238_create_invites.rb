class CreateInvites < ActiveRecord::Migration
  def change
    create_table :invites do |t|
      t.integer :invitee_id
      t.integer :inviter_id
      t.integer :group_id
      t.timestamps
    end
  end
end
