class AddPhoneToUsers < ActiveRecord::Migration
  def change
    add_column :users, :area_code, :string
    add_column :users, :phone_number, :string
  end
end
