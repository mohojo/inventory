class AddInputsTable < ActiveRecord::Migration
  def change
    create_table :inputs do |t|
      t.integer :group_id
      t.integer :product_id
      t.integer :quantity
      t.datetime :input_time
      t.timestamps
    end
  end
end
